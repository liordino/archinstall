#!/bin/bash

function print() {
    echo -e $1
}

function print_line_separator() {
    printf "%$(tput cols)s\n"|tr ' ' '-'
}

function set_the_keyboard_layout() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Set_the_keyboard_layout
    loadkeys $1
}

function verify_the_boot_mode() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Verify_the_boot_mode
    if [[ -d "/sys/firmware/efi/efivars" ]]; then
        print "Detected boot mode: UEFI"
        boot_mode=0
    else
        print "Detected boot mode: BIOS"
        boot_mode=1
    fi
}

function connect_to_the_internet() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Connect_to_the_Internet
    wget -q --spider archlinux.org

    if [ $? -eq 0 ]; then
        return 0
    else
        return 1
    fi
}

function update_the_system_clock() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Update_the_system_clock
    timedatectl set-ntp true
}

function partition_the_disks() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Partition_the_disks
    swap_partition_size_in_gb=$(expr $(expr $(dmesg | awk '/Memory\:/ { print $4 }' | cut -d'K' -f 2 | tail -c +2) / 1000000) \* 2)
    hard_disks_names=($(lsblk -I 8 -d -n -o NAME))    

    delete_all_partitions
    
    if (( $1 == 0 )); then 
        # TODO
        print "GUID partitioning not implemented yet..."
        exit_script 1
        #   Mount point	    Partition	Partition type GUID	                                        Size
        #   /boot	        /dev/sda1	C12A7328-F81F-11D2-BA4B-00A0C93EC93B: EFI System Partition  550 MiB
        #   /	            /dev/sda2	4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709: Linux x86-64 root (/) 23 - 32 GiB
        #   [SWAP]	        /dev/sda3	0657FD6D-A4AB-43C4-84E5-0933C84B4F4F: Linux swap            2 * RAM
        #   /home	        /dev/sda4	933AC7E1-2EB4-4F13-B844-0E14E2AEF915: Linux /home		    Remainder of the device
        #   /data (?)       /dev/sdb1   933AC7E1-2EB4-4F13-B844-0E14E2AEF915: Linux /data (?)       The entire device
    else
        print "Partitioning disk /dev/sda..." 
            (
                echo o # clear the in memory partition table
                echo n # new partition
                echo p # primary partition
                echo 1 # root partition, number 1
                echo   # default - start at beginning of disk 
                echo +${root_partition_size_in_gb}G # root partition size
                echo n # new partition
                echo p # primary partition
                echo 2 # swap partition, number 2
                echo   # default, start immediately after preceding partition
                echo +${swap_partition_size_in_gb}G #swap partition size
                echo t # change partition type...
                echo   # change current partition
                echo 82 # ... to Linux swap / Solaris
                echo n # new partition
                echo p # primary partition
                echo 3 # home partition, number 3
                echo   # default, start immediately after preceding partition
                echo   # default, extend partition to end of disk
                echo a # make a partition bootable
                echo 1 # bootable partition is partition 1 -- /dev/sda1
                echo w # write the partition table
                echo q # finish
            ) | sudo fdisk /dev/sda

        # Partition the rest of the disks, skipping the first one on the list, since it was already partitioned
        for disk_name in "${hard_disks_names[@]:1}"
        do
            print "Partitioning disk /dev/${disk_name}..."
                (
                    echo o # clear the in memory partition table
                    echo n # new partition
                    echo p # primary partition
                    echo 1 # partition number 1
                    echo   # default - start at beginning of disk 
                    echo   # default, extend partition to end of disk
                    echo w # write the partition table
                    echo q # finish
                ) | sudo fdisk /dev/${disk_name}
        done
    fi
}

function delete_all_partitions() {
    partition_names=($(lsblk -I 8 -n -r | grep 'part' | awk '{ print $1 }'))
    mounting_points=($(lsblk -I 8 -n -r | grep 'part' | awk '{ print $7 }'))

    swapoff -a

    for each in "${partition_names[@]}"
    do
        umount /dev/${each}
    done

    for each in "${mounting_points[@]}"
    do
        rmdir ${each}
    done

    for each in "${hard_disks_names[@]}"
    do
        wipefs -a /dev/${each}
        print "- Deleted all data from ${each}"
    done
}

function format_the_partitions() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Format_the_partitions
    if (( $1 == 0 )); then 
        # TODO
        print "GUID partition formating not implemented yet..."
        exit_script 1
    else
        print "Formating /dev/sda partitions..."
        mkfs.btrfs /dev/sda1
        mkswap /dev/sda2
        swapon /dev/sda2
        mkfs.btrfs /dev/sda3

        for disk_name in "${hard_disks_names[@]:1}"
        do
            print "Formating /dev/${disk_name} partitions..."
            mkfs.btrfs /dev/${disk_name}1
        done
    fi
}

function mount_the_file_systems() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Mount_the_file_systems
    if (( $1 == 0 )); then 
        # TODO
        print "GUID file systems mounting not implemented yet..."
        exit_script 1
    else
        print "Mounting /dev/sda partitions..."
        mount /dev/sda1 /mnt
        mkdir /mnt/home
        mount /dev/sda3 /mnt/home

        for disk_name in "${hard_disks_names[@]:1}"
        do
            print "Mounting /dev/${disk_name} partitions..."
            mkdir /mnt/${disk_name}
            mount /dev/${disk_name}1 /mnt/${disk_name}
        done
    fi
}

function select_the_mirrors() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Select_the_mirrors
    # Disclaimer: all code on this function got from https://github.com/helmuthdu/aui
    url="https://www.archlinux.org/mirrorlist/?country=$1&use_mirror_status=on"
    tmpfile=$(mktemp --suffix=-mirrorlist)

    # Get latest mirror list and save to tmpfile
    curl -so ${tmpfile} ${url}
    sed -i 's/^#Server/Server/g' ${tmpfile}

    # Backup and replace current mirrorlist file (if new file is non-zero)
    if [[ -s ${tmpfile} ]]; then
        { mv -i /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.orig; } &&
        { mv -i ${tmpfile} /etc/pacman.d/mirrorlist; }
    fi
  
    # better repo should go first
    cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.tmp
    rankmirrors /etc/pacman.d/mirrorlist.tmp > /etc/pacman.d/mirrorlist
    rm /etc/pacman.d/mirrorlist.tmp
  
    # allow global read access (required for non-root yaourt execution)
    chmod +r /etc/pacman.d/mirrorlist
}

function install_the_base_packages() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Install_the_base_packages
    pacstrap /mnt base
}

function generate_fstab() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Fstab
    if (( $1 == 0 )); then 
        genfstab -U /mnt >> /mnt/etc/fstab
    else
        genfstab -L /mnt >> /mnt/etc/fstab
    fi
}

function change_root() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Chroot
    arch-chroot /mnt
}

function set_the_time_zone() {
    # https://wiki.archlinux.org/index.php/Installation_guide#Time_zone
    timedatectl set-timezone $1/$2
    hwclock --systohc
}

function exit_script() {
    print "Exiting script..."
    exit $1
}
