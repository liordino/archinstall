# Arch Linux Installer
My take on a Arch Linux installation script.  
Heavily inspired on [helmuthdu's aui](https://github.com/helmuthdu/aui) and [LukeSmithxyz's LARBS](https://github.com/LukeSmithxyz/LARBS).

## Usage
- Get the script `wget https://github.com/liordino/archinstall/tarball/master -O - | tar xz`
- Make the main script executable `chmod +x <dir>/install`
- Be aware that it will delete everything
- Run it `cd <dir> && ./install`

## What it does
The goal here is to pretty much follow the official [Arch Linux Installation Guide](https://wiki.archlinux.org/index.php/Installation_guide) with my personal preferences and after that setup my personal workstation. As of now the script performs the following operations, in order:
- Set the keyboard layout to `br-abnt2`
- Verify the machine's boot mode
- Check the internet connection (and exit if it's not available)
- Update the system clock
- Partition the disks
    - Delete old partitions
    - Disk /dev/sda is divided in 3 partitions:
        - root: maximum of 34GB
        - SWAP: 2 * machine RAM 
        - home: the remainder of the disk
    - Other disks:
        - A single partition that uses the entire disk
- Format the partitions with the `btrfs` file system
- Mount the file systems
- Select the mirrors from country code `BR (Brazil)`
- Install the base packages